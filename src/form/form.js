import '../assets/styles/styles.scss';
import './form.scss';
import {
  openModal
} from '../assets/js/modal';

const form = document.querySelector("form");
const errorElement = document.querySelector("#errors");
const btnCancel = document.querySelector(".btn-secondary");
let articleId;
let errors = [];

const fillForm = article => {
  const author = document.querySelector('input[name="author"]');
  const img = document.querySelector('input[name="img"]');
  const category = document.querySelector('input[name="category"]');
  const title = document.querySelector('input[name="title"]');
  const content = document.querySelector('textarea');
  author.value = article.author || '';
  img.value = article.img || '';
  category.value = article.category || '';
  title.value = article.title || '';
  content.value = article.content || '';
}

const initForm = async () => {
  const params = new URL(location.href);
  articleId = params.searchParams.get('id');
  if (articleId) {
    const response = await await fetch(`https://restapi.fr/api/articlesjd/${ articleId }`);
    if (response.status < 300) {
      const article = await response.json();
      fillForm(article);
    }
  }
}

initForm();

btnCancel.addEventListener('click', async () => {
  const result = await openModal(
    'Si vous quittez la page, vous perdrez votre article'
  );
  if (result) {
    window.location.assign('/index.html');
  }
});

form.addEventListener("submit", async event => {
  event.preventDefault();
  const formData = new FormData(form);
  const article = Object.fromEntries(formData.entries());
  if (formIsValid(article)) {
    try {
      const json = JSON.stringify(article);
      let response;
      if (articleId) {
        response = await fetch(`https://restapi.fr/api/articlesjd/${articleId}`, {
          method: "PATCH",
          body: json,
          headers: {
            "Content-Type": "application/json"
          }
        });
      } else {
        response = await fetch("https://restapi.fr/api/articlesjd", {
          method: "POST",
          body: json,
          headers: {
            "Content-Type": "application/json"
          }
        });
      }
      const body = await response.json();
      console.log(body);
      if (response.status < 299) {
        window.location.assign("/index.html");
      }
    } catch (e) {
      console.error("e : ", e);
    }
  }
});

const formIsValid = (article) => {
  errors = [];
  if (!article.author || !article.category || !article.content || !article.img || !article.title) {
    if (!article.author) {
      errors.push("Vous n'avez pas renseigner le champs auteur");
    }
    if (!article.category) {
      errors.push("Vous n'avez pas renseigner le champs catégorie");
    }
    if (!article.content) {
      errors.push("Vous n'avez pas renseigner le champs article");
    }
    if (!article.img) {
      errors.push("Vous n'avez pas ajouté d'image");
    }
    if (!article.title) {
      errors.push("Vous n'avez pas renseigner le champs titre");
    }
  } else {
    errors = [];
  }
  //   if (!article.author || !article.category || !article.content) {
  //     errors.push("Vous n'avez pas renseigner tous les champs");
  //   } else {
  //     errors = [];
  //   }
  if (errors.length) {
    let errorHtml = '';
    errors.forEach((e) => {
      console.log(e)
      errorHtml += `<li>${e}</li>`;
    })
    errorElement.innerHTML = errorHtml;
    return false;
  } else {
    errorElement.innerHTML = '';
    return true;
  }
}